import utfpr.ct.dainf.if62c.pratica.Matriz;

/**
 * IF62C Fundamentos de Programação 2
 * Exemplo de programação em Java.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica33 {
    
    public static void main(String[] args) {
        Matriz orig = new Matriz(2, 2);
        Matriz nova = new Matriz(2, 2);
        double[][] m = orig.getMatriz();
        m[0][0] = 1;
        m[0][1] = 2;
        m[1][0] = 3;
        m[1][1] = 4;
        
        double[][] n = nova.getMatriz();
        n[0][0] = 1;
        n[0][1] = 2;
        n[1][0] = 3;
        n[1][1] = 4;
        
        

        Matriz soma = orig.soma(nova);
        Matriz prod = orig.prod(nova);
        System.out.println(orig);
        System.out.println(nova);
        System.out.println(soma);
        System.out.println(prod);
    }
}
